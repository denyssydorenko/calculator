package com.sanitas.calculator.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sanitas.calculator.dto.CalculatorDTO;
import com.sanitas.calculator.service.CalculatorService;

@RestController
@RequestMapping("/api")
public class CalculatorController {
	
	@Autowired
	private CalculatorService calculatorService;
	
	@GetMapping("/calculator")
	public ResponseEntity<BigDecimal> calculateNumber(@RequestBody CalculatorDTO calculatorParams) {
		if(calculatorService.calculatorFunction(calculatorParams) != null) {
			return new ResponseEntity<BigDecimal>(calculatorService.calculatorFunction(calculatorParams), HttpStatus.OK);
		}
		else {
			return new ResponseEntity<BigDecimal>(HttpStatus.NOT_FOUND);
		}
	}

}
