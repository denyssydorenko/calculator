package com.sanitas.calculator.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.sanitas.calculator.dto.CalculatorDTO;

@Service
public class CalculatorService {
	
	public BigDecimal calculatorFunction(CalculatorDTO calculatorParams) {
		BigDecimal result = BigDecimal.ZERO;
		try {
			switch(calculatorParams.getFunction()) {
				case "add":
					result = calculatorParams.getNumber1().add(calculatorParams.getNumber2());
					break;
				case "sub":
					result = calculatorParams.getNumber1().subtract(calculatorParams.getNumber2());
					break;
				case "mul":
					result = calculatorParams.getNumber1().multiply(calculatorParams.getNumber2());
					break;
				case "div":
					result = calculatorParams.getNumber1().divide(calculatorParams.getNumber2());
					break;
				default:
					break;
			}
			return result;
		}
		catch(ArithmeticException e) {
			return null;
		}
	}

}
